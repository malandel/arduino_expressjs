// Instanciation des variables
let message = ""
let distance = ""
let distance_int = 0
let distance_securite = 4000

// Connexion au port utilisé par Arduino
const SerialPort = require('serialport')
const Readline = SerialPort.parsers.Readline
const port = new SerialPort('/dev/ttyUSB1', {
  baudRate: 9600
})

const parser = new Readline()

// Lecture et traitement des données
port.pipe(parser)
parser.on('data', function (data) {
  console.log('Data:', data)
  numeros = data.match(/\d/g);
  numeros = numeros.join("");
  if ( numeros < distance_securite){
    message = "Vous êtes trop près ! Merci de respecter les gestes barrières !"
  }
  else {
    message = "C'est bien. Restez loin."
  }
  return distance = data, distance_int = numeros
})

// Serveur express
let express = require('express');
let server= express();
const portServer = process.env.PORT || 8080;

//Initialisation du templating EJS
server.set("view engine", "ejs");
server.use(express.static(__dirname + '/static'));

// Routage, Envoi des données au template
server.get('/', function(request, response) {
  response.render('index', {message: message, distance: distance, numeros:distance_int, distance_securite: distance_securite});
});
server.listen(portServer);

console.log('Server started at http://localhost:' + portServer);