# People Detector : Détection des distances de sécurité

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/open-source.svg)](https://forthebadge.com)

Un projet d'électronique embarquée avec Arduino et son application associée avec NodeJs et ExpressJs.

Détection des mouvements devant le capteur et alerte si la distance minimum n'est pas respectée.

### Pré-requis

#### Partie IoT
- Installer le logiciel [Arduino](https://www.arduino.cc/en/software)
- Hardware : une carte Arduino, une breadboard, un [capteur ultrason](https://www.carnetdumaker.net/articles/mesurer-une-distance-avec-un-capteur-ultrason-hc-sr04-et-une-carte-arduino-genuino/), une [diode RGB](https://create.arduino.cc/projecthub/muhammad-aqib/arduino-rgb-led-tutorial-fc003e), câbles et résistances.

#### Partie applicative
- [NodeJs](https://nodejs.org/en/) - v14.17.6
- [npm](https://www.npmjs.com/)

### Montage électronique

- Suivre ces 2 tutoriels pour monter le [capteur](https://www.carnetdumaker.net/articles/mesurer-une-distance-avec-un-capteur-ultrason-hc-sr04-et-une-carte-arduino-genuino/) et la [LED](https://create.arduino.cc/projecthub/muhammad-aqib/arduino-rgb-led-tutorial-fc003e) 
- Téléchargez le programme "./capteur_et_led.ino"et l'ouvrir dans l'éditeur Arduino.
- Branchez votre carte, téléverser le programme.

### Installation de l'application

```
git clone https://gitlab.com/malandel/arduino_expressjs.git
cd arduino_expressjs
npm install
```

- Dans le fichier server.js, l.10 renseigner le port correspondant à votre configuration (L'onglet 'Outils > Port' dans le logiciel Arduino)

## Démarrage

- ```npm run watch``` pour lancer le serveur avec [Nodemon](https://nodemon.io/)
- ```npm run start``` pour lancer le serveur classique avec NodeJS
- http://localhost:8080 pour ouvrir l'application





