 /* CAPTEUR */
 
 const byte TRIGGER_PIN = 2;
 const byte ECHO_PIN = 3;
 const unsigned long MEASURE_TIMEOUT = 25000UL;
 const float SOUND_SPEED = 340.0 / 1000;

 /* LED */

 int red_light_pin= 11;
 int green_light_pin = 10;
 int blue_light_pin = 9;

  
void setup() {

  /* CAPTEUR */
  Serial.begin(9600);
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW);
  pinMode(ECHO_PIN, INPUT);

  /* LED */
  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);
  
}

void loop() {

  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);

  /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
  long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);

  /* 3. Calcule la distance à partir du temps mesuré */
  float distance_mm = measure / 2.0 * SOUND_SPEED ;

  /* '. Affiche les résultats en mm, cm et m */
  Serial.print("Distance : ");
  Serial.print(distance_mm / 10.0, 2);
  Serial.print("cm ");
  Serial.print("\n");

  if (distance_mm < 100.0){
    /* red */
    RGB_color(255, 0, 0);
    delay(1000);
  }
  else if (distance_mm < 500.0 && distance_mm >= 100.0){
    /* green */
    RGB_color(0, 255, 0);
    delay(1000);
  }
  else if(distance_mm < 1000.0 && distance_mm >= 500.0){
    /* blue */
    RGB_color(0, 0, 255);
    delay(1000);
  }
  else{
    RGB_color(0, 0, 0);
    delay(1000);
  }

  /* Délai d'attente pour éviter d'afficher trop de résultats à la seconde */
  delay(500);

}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(red_light_pin, red_light_value);
  analogWrite(green_light_pin, green_light_value);
  analogWrite(blue_light_pin, blue_light_value);
}
